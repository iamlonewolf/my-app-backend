/**
 * Required External Modules
 */
import express from "express";
import cors from "cors";
import helmet from "helmet";

import config from "./config/config";
import initDb from "./db";
import routes from "./routes/index";
/**
 * App Variables
 */
const app = express();

/**
 *  App Configuration
 */
if (!config.serverPort) {
  process.exit(1);
}
// dotenv.config();

/***
 * Routes
 */
app.use(helmet());
app.use(
  cors({
    origin: "*",
  })
);
app.use(express.json());
app.use("/api", routes);

/**
 * Server Activation
 */
app.listen(config.serverPort, () => {
  console.log(`listening to port ${config.serverPort}`);
});

// initialize database
initDb();
