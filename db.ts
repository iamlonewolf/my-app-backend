import config from "./config/config";
import { connect, connection } from "mongoose";
import { isUndefined } from "lodash";

export default function () {
  connection.on("connecting", () => {
    console.log("connecting to database..");
  });

  connection.on("open", () => {
    console.log("successfully connected to database.");
  });
  connection.on("error", () => {
    console.log(
      "failed connecting to database. Please check your configuration settings."
    );
  });

  if (isUndefined(config.dbConnectionString)) {
    return;
  }

  connect(config.dbConnectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
}
