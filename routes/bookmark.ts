import { Router } from "express";
import { constants } from "http2";
import authorization from "../middleware/authorization";
import { head, isEmpty } from "lodash";
const {
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_CREATED,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
  HTTP_STATUS_NOT_FOUND,
} = constants;
import {
  addBookmark,
  deleteAccountBookmark,
  getBookmark,
  getBookmarksByFilter,
  getBookmarksCountByFilter,
  updateBookmark,
  validateBookmark,
  BookmarkModel,
} from "../models/schemas/Bookmark";
import { errorExtractor } from "../utilities/response";
import {
  validateAccountBookmarkCountParams,
  validateAccountBookmarkParams,
} from "../models/schemas/validators/AccountBookmark";
import { AccountBookmark } from "../models/interfaces/validators/AccountBookmark";
import { HEADER_KEY_COUNT } from "../middleware/headers";

const router = Router();

router.get("/:bookmarkId", [authorization], async (req, res) => {
  try {
    const result = await getBookmark(req.params.bookmarkId);
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_NOT_FOUND).send("");
  }
});

router.put("/:bookmarkId", [authorization], async (req, res) => {
  try {
    const { error } = validateBookmark(req?.body);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await updateBookmark(req.params.bookmarkId, req.body);
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_BAD_REQUEST).json(err);
  }
});

router.post("/", [authorization], async (req, res) => {
  try {
    const { error } = validateBookmark(req?.body);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await addBookmark(req.account._id, req.body);
    res.status(HTTP_STATUS_CREATED).json(result);
  } catch (error) {
    if (error?.errors) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .send(errorExtractor(error?.errors, ["name", "password", "email"]));
    }
    res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR).send(error);
  }
});

router.get("/account/:accountId/v1", async (req, res) => {
  try {
    const { error } = validateAccountBookmarkParams(req.query);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await getBookmarksByFilter(req.params.accountId, req.query);
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_NOT_FOUND).send();
  }
});

router.get("/account/:accountId", async (req, res) => {
  try {
    const { error } = validateAccountBookmarkParams(req.query);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await getBookmarksByFilter(req.params.accountId, req.query);
    const count = await getBookmarksCountByFilter(
      req.params.accountId,
      req.query
    );
    res.header(HEADER_KEY_COUNT, count?.toString()).json(result);
  } catch (err) {
    res.status(HTTP_STATUS_NOT_FOUND).send();
  }
});

router.get("/account/:accountId/count", async (req, res) => {
  const params: AccountBookmark = req.query;
  try {
    const { error } = validateAccountBookmarkCountParams(req.query);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await getBookmarksCountByFilter(
      req.params.accountId,
      params
    );
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_NOT_FOUND).send();
  }
});

router.delete("/:bookmarkId", [authorization], async (req, res) => {
  try {
    const result = await deleteAccountBookmark(
      req.account._id,
      req.params.bookmarkId
    );
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_BAD_REQUEST).json([]);
  }
});

export default router;
