import { Router } from "express";
import headers from "../middleware/headers";
import identityRoutes from "./identity";
import bookmarkRoutes from "./bookmark";
import categoryRoutes from "./category";
const router = Router();

router.use(headers);
router.use("/identity", identityRoutes);
router.use("/category", categoryRoutes);
router.use("/bookmark", bookmarkRoutes);

router.get("/", (req, res) => {
  res.send("MyApp API");
});

export default router;
