import { Router } from "express";
import { constants } from "http2";
const { HTTP_STATUS_NOT_FOUND, HTTP_STATUS_BAD_REQUEST, HTTP_STATUS_CREATED } =
  constants;
import { head, isEmpty } from "lodash";
import authorization from "../middleware/authorization";
import { validateAccountCategoryParams } from "./../models/schemas/validators/AccountCategory";
import {
  addAccountCategory,
  deleteAccountCategory,
  getAccountCategoriesByFilter,
  getAccountCategoriesCountByFilter,
  getCategory,
  updateCategory,
  validateCategory,
} from "../models/schemas/Category";
import { HEADER_KEY_COUNT } from "../middleware/headers";
const router = Router();

router.get("/account/:accountId", [authorization], async (req, res) => {
  try {
    const { error } = validateAccountCategoryParams(req.query);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }

    const result = await getAccountCategoriesByFilter(
      req?.params?.accountId,
      req?.query
    );
    const count = await getAccountCategoriesCountByFilter(
      req?.params?.accountId,
      req?.query
    );
    if (isEmpty(result)) {
      res.status(HTTP_STATUS_NOT_FOUND).send();
      return;
    }
    res.header(HEADER_KEY_COUNT, count).json(result);
    res.send(result);
  } catch (error) {
    res.status(HTTP_STATUS_BAD_REQUEST).send();
  }
});

router.get("/:categoryId", async (req, res) => {
  try {
    const result = await getCategory(req.params.categoryId);
    if (isEmpty(result)) {
      res.status(HTTP_STATUS_NOT_FOUND).send();
      return;
    }
    res.send(result);
  } catch (error) {
    res.status(HTTP_STATUS_BAD_REQUEST).send();
  }
});

router.post("/", [authorization], async (req, res) => {
  try {
    console.log("adding bookmark category", req.body);
    const { error } = validateCategory(req?.body);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await addAccountCategory(req.account._id, req.body);
    res.status(HTTP_STATUS_CREATED).json(result);
  } catch (error) {}
});

router.put("/:categoryId/", [authorization], async (req, res) => {
  try {
    const { error } = validateCategory(req?.body);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const result = await updateCategory(req.params.categoryId, req.body);
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_BAD_REQUEST).json([]);
  }
});

router.delete("/:categoryId", [authorization], async (req, res) => {
  try {
    const result = await deleteAccountCategory(
      req.account._id,
      req.params.categoryId
    );
    res.json(result);
  } catch (err) {
    res.status(HTTP_STATUS_BAD_REQUEST).json([]);
  }
});

export default router;
