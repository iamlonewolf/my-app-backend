import Joi from "joi";
import { DEFAULT_MAX_PAGE_SIZE_LENGTH } from "../../common";
import { DEFAULT_MAX_SEARCH_LENGTH } from "../../common";
import { AccountCategory } from "../../interfaces/validators/AccountCategory";

export const validateAccountCategoryParams = (params: AccountCategory) => {
  const schema = Joi.object({
    searchText: Joi.string().allow("").max(DEFAULT_MAX_SEARCH_LENGTH),
    pageSize: Joi.number().max(DEFAULT_MAX_PAGE_SIZE_LENGTH),
    page: Joi.number(),
  });
  return schema.validate(params);
};

export const validateAccountCategoryCountParams = (params: AccountCategory) => {
  const schema = Joi.object({
    searchText: Joi.string().allow("").max(DEFAULT_MAX_SEARCH_LENGTH),
  });
  return schema.validate(params);
};
