import Joi from "joi";
import { DEFAULT_MAX_PAGE_SIZE_LENGTH } from "../../common";
import { DEFAULT_MAX_SEARCH_LENGTH } from "../../common";
import { AccountBookmark } from "../../interfaces/validators/AccountBookmark";

export const validateAccountBookmarkParams = (params: AccountBookmark) => {
  const schema = Joi.object({
    searchText: Joi.string().allow("").max(DEFAULT_MAX_SEARCH_LENGTH),
    pageSize: Joi.number().max(DEFAULT_MAX_PAGE_SIZE_LENGTH),
    page: Joi.number(),
    category: Joi.string(),
  });
  return schema.validate(params);
};

export const validateAccountBookmarkCountParams = (params: AccountBookmark) => {
  const schema = Joi.object({
    searchText: Joi.string().allow("").max(DEFAULT_MAX_SEARCH_LENGTH),
  });
  return schema.validate(params);
};
