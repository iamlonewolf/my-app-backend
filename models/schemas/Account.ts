import { Schema, model, models } from "mongoose";
import { genSalt, hash } from "bcrypt";
import { sign } from "jsonwebtoken";
import config from "../../config/config";
import { Account } from "../interfaces/Account";
import Joi from "joi";

const DEFAULT_MAX_LENGTH = 255;
const DEFAULT_MIN_LENGTH = 5;

export const accountSchema = new Schema<Account>(
  {
    name: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
    },
    email: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
      max: DEFAULT_MAX_LENGTH,
    },
    password: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
      max: DEFAULT_MAX_LENGTH,
      select: false,
    },
    dateCreated: {
      type: String,
    },
    bookmarks: [
      {
        type: Schema.Types.ObjectId,
        ref: "Bookmark",
      },
    ],
    categories: [
      {
        type: Schema.Types.ObjectId,
        ref: "Categories",
      },
    ],
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
    toJSON: { virtuals: true },
  }
);

export const generateAuthToken = function ({ _id, name, email }: Account) {
  return sign(
    {
      _id,
      name,
      email,
    },
    config.jwtPrivateKey
  );
};

accountSchema.virtual("bookmarkCount", {
  ref: "Bookmark",
  localField: "bookmarks",
  foreignField: "_id",
  count: false,
});

export const AccountModel =
  models.Account ?? model<Account>("Account", accountSchema);

export async function createUser(account: Account): Promise<Account> {
  await AccountModel.init();
  const accountModel = new AccountModel(account);
  const salt = await genSalt(config.passwordSalt);
  accountModel.password = await hash(account?.password, salt);
  return await accountModel.save();
}

export async function getAccountByEmail(email: string): Promise<Account> {
  return await AccountModel.findOne({ email }).select("password");
}

export async function getAccount(id: string): Promise<Account> {
  return await AccountModel.findById(id);
}

export const validateAccount = (account: Account) => {
  const schema = Joi.object({
    name: Joi.string().min(DEFAULT_MIN_LENGTH).required(),
    email: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required()
      .email({ tlds: { allow: false } }),
    password: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required(),
  });
  return schema.validate(account);
};

export const validateLogin = (email: string, password: string) => {
  const schema = Joi.object({
    email: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required()
      .email({ tlds: { allow: false } }),
    password: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required(),
  });
  return schema.validate({ email, password });
};
