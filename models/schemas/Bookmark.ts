import { Schema, model, models, FilterQuery } from "mongoose";
import Joi from "joi";
import { Bookmark } from "./../interfaces/Bookmark";
import { AccountModel } from "./../schemas/Account";
import { isUndefined, toNumber } from "lodash";
import { AccountBookmark } from "../interfaces/validators/AccountBookmark";
import { DEFAULT_MAX_PAGE_SIZE_LENGTH } from "../common";

const DEFAULT_MAX_LENGTH = 500;
const DEFAULT_MIN_LENGTH = 5;

export const bookmarkSchema = new Schema<Bookmark>(
  {
    account: {
      type: Schema.Types.ObjectId,
      ref: "Account",
    },
    title: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
    },
    url: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
      max: DEFAULT_MAX_LENGTH,
    },
    description: {
      type: String,
      max: DEFAULT_MAX_LENGTH,
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: "Category",
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
  }
);

bookmarkSchema.post("remove", { document: false, query: true }, function (doc) {
  console.log("bookmark removed");
});

bookmarkSchema.pre("save", function () {
  console.log("bookmark saved");
});

export const BookmarkModel =
  models.Bookmark ?? model<Bookmark>("Bookmark", bookmarkSchema);

/**
 * @param bookmarkId
 * @returns
 */
export async function getBookmark(
  bookmarkId: string
): Promise<Bookmark | undefined> {
  return await BookmarkModel.findById(bookmarkId)
    .populate("category", "-__v")
    .select("-__v -account");
}

/**
 * @param accountId
 * @returns returns bookmarks based from accountId
 */
export async function getBookmarksByAccountId(
  accountId: string
): Promise<Bookmark[] | undefined> {
  const query: FilterQuery<any> = {
    account: accountId,
  };
  return await BookmarkModel.find(query).select("-__v").sort("-createdAt");
}

/**
 *
 * @param accountId
 * @param searchText
 * @returns bookmark based from the searched title
 */
export async function getBookmarksByFilter(
  accountId: string,
  params: AccountBookmark
): Promise<Bookmark[] | undefined> {
  let query: FilterQuery<any> = {
    account: accountId,
  };

  if (params?.category) {
    query = { ...query, category: params?.category };
  }

  if (!isUndefined(params?.searchText)) {
    query = {
      ...query,
      title: {
        $regex: `.*${params?.searchText}.*`,
        $options: "i",
      },
    };
  }

  const PAGE = toNumber(params?.page) ?? 0;
  const PAGE_SIZE = toNumber(params?.pageSize ?? DEFAULT_MAX_PAGE_SIZE_LENGTH);

  return await BookmarkModel.find(query)
    .populate("category", "-__v")
    .select("-__v")
    .skip(PAGE_SIZE * PAGE)
    .limit(PAGE_SIZE)
    .sort("-createdAt");
}

/**
 *
 * @param accountId
 * @returns
 */
export async function getBookmarksCountByFilter(
  accountId: string,
  params: AccountBookmark
): Promise<number | undefined> {
  let query: FilterQuery<any> = {
    account: accountId,
  };

  if (params?.category) {
    query = { ...query, category: params?.category };
  }

  if (!isUndefined(params?.searchText)) {
    query = {
      ...query,
      title: {
        $regex: `.*${params?.searchText}.*`,
        $options: "i",
      },
    };
  }
  return await BookmarkModel.find(query).countDocuments();
}
/**
 * @param accountId
 * @param bookmark
 * @returns
 */
export async function addBookmark(
  accountId: string,
  bookmark: Bookmark
): Promise<Bookmark> {
  const query: FilterQuery<any> = { _id: accountId };
  const account = await AccountModel.findOne(query);
  const bookmarkModel = new BookmarkModel(bookmark);
  bookmarkModel.account = accountId;
  await bookmarkModel.save();
  account.bookmarks.push(bookmarkModel);
  return await account.save();
}

/**
 * @param bookmarkId
 * @param bookmark
 * @returns
 */
export async function updateBookmark(
  bookmarkId: string,
  bookmark: Bookmark
): Promise<Bookmark> {
  // @FIXME Find better way to delete category reference
  if (!bookmark?.category) {
    bookmark.category = undefined;
  }

  const query: FilterQuery<any> = {
    _id: bookmarkId,
  };
  // @ts-ignore
  return await BookmarkModel.findOneAndUpdate(query, bookmark);
}

/**
 * @param bookmarkId
 * @returns
 */
export async function deleteAccountBookmark(
  accountId: number,
  bookmarkId: string
): Promise<Bookmark> {
  await BookmarkModel.remove({ _id: bookmarkId });
  console.log(
    `deleting bookmark ${bookmarkId} belongs to account ${accountId}`
  );
  return await AccountModel.findById(accountId, function (err, doc) {
    if (err) {
      console.log(
        "there is a problem in finding account for bookmark deletion",
        err
      );
      return;
    }
    doc.bookmarks.pull(bookmarkId);
    doc.save();
  });
}

export const validateBookmark = (bookmark: Bookmark) => {
  const schema = Joi.object({
    title: Joi.string()
      .required()
      .max(DEFAULT_MAX_LENGTH)
      .min(DEFAULT_MIN_LENGTH),
    url: Joi.string()
      .required()
      .max(DEFAULT_MAX_LENGTH)
      .min(DEFAULT_MIN_LENGTH),
    description: Joi.string().max(DEFAULT_MAX_LENGTH).allow(""),
    category: Joi.string().allow(""),
    createdAt: Joi.string().allow(""),
    updatedAt: Joi.string().allow(""),
  });
  return schema.validate(bookmark);
};
