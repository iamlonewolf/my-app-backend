import Joi from "joi";
import { isUndefined, toNumber } from "lodash";
import { Schema, model, models, FilterQuery } from "mongoose";
import { DEFAULT_MAX_PAGE_SIZE_LENGTH } from "../common";
import { Category } from "../interfaces/Category";
import { AccountCategory } from "../interfaces/validators/AccountCategory";
import { AccountModel } from "./Account";

const DEFAULT_MAX_LENGTH = 500;
const DEFAULT_MIN_LENGTH = 1;

export const categorySchema = new Schema<Category>({
  name: {
    type: String,
    required: true,
    min: DEFAULT_MIN_LENGTH,
  },
  description: {
    type: String,
    required: false,
    max: DEFAULT_MAX_LENGTH,
  },
  account: {
    type: Schema.Types.ObjectId,
    ref: "Account",
  },
});

export const CategoryModel =
  models.Category ?? model("Category", categorySchema);

export async function getAccountCategories(
  accountId: string
): Promise<Category[]> {
  const query: FilterQuery<any> = {
    account: accountId,
  };
  return await CategoryModel.find(query).select("-__v");
}

/**
 *
 * @param accountId
 * @param params
 * @returns
 */
export async function getAccountCategoriesByFilter(
  accountId: string,
  params: AccountCategory
): Promise<Category[]> {
  let query: FilterQuery<any> = {
    account: accountId,
  };

  if (!isUndefined(params?.searchText)) {
    query = {
      ...query,
      name: {
        $regex: `.*${params?.searchText}.*`,
        $options: "i",
      },
    };
  }

  const PAGE = toNumber(params?.page) ?? 0;
  const PAGE_SIZE = toNumber(params?.pageSize ?? DEFAULT_MAX_PAGE_SIZE_LENGTH);

  return await CategoryModel.find(query)
    .select("-__v")
    .skip(PAGE_SIZE * PAGE)
    .limit(PAGE_SIZE);
}

export async function getAccountCategoriesCountByFilter(
  accountId: string,
  params: AccountCategory
): Promise<number> {
  let query: FilterQuery<any> = {
    account: accountId,
  };

  if (!isUndefined(params?.searchText)) {
    query = {
      ...query,
      name: {
        $regex: `.*${params?.searchText}.*`,
        $options: "i",
      },
    };
  }

  return await CategoryModel.find(query).countDocuments();
}

/**
 * @param categoryId
 * @returns
 */
export async function getCategory(
  categoryId: string
): Promise<Category | undefined> {
  return await CategoryModel.findById(categoryId).select("-__v -account");
}

/**
 * @param accountId
 * @param category
 * @returns
 */
export async function addAccountCategory(
  accountId: string,
  category: Category
): Promise<Category> {
  const query: FilterQuery<any> = { _id: accountId };
  const account = await AccountModel.findOne(query);
  const categoryModel = new CategoryModel(category);
  categoryModel.account = accountId;
  await categoryModel.save();
  account.categories.push(categoryModel);
  return await account.save();
}

/**
 * @param categoryId
 * @param category
 * @returns
 */
export async function updateCategory(
  categoryId: string,
  category: Category
): Promise<Category> {
  console.log(categoryId, category);
  return await CategoryModel.findByIdAndUpdate(categoryId, category);
}

export async function deleteAccountCategory(
  accountId: string,
  categoryId: string
): Promise<Category> {
  const query: FilterQuery<any> = { _id: categoryId };
  await CategoryModel.deleteOne(query);
  return await AccountModel.findById(accountId, function (err, doc) {
    if (err) {
      console.log(
        "there is a problem in finding account for category deletion",
        err
      );
      return;
    }
    doc.categories.pull(categoryId);
    doc.save();
  });
}

export const validateCategory = (category: Category) => {
  const schema = Joi.object({
    name: Joi.string()
      .required()
      .max(DEFAULT_MAX_LENGTH)
      .min(DEFAULT_MIN_LENGTH),
    description: Joi.string().allow("").max(DEFAULT_MAX_LENGTH),
  });
  return schema.validate(category);
};
