import { Bookmark } from "./Bookmark";
export interface Account {
  _id?: number;
  name?: string;
  email?: string;
  password?: string;
  bookmarks?: Bookmark[];
  dateCreated?: string;
  dateUpdated?: string;
}
