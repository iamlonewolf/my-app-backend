export interface AccountBookmark {
  searchText?: string;
  page?: number;
  pageSize?: number;
  sort?: number;
  category?: string;
}

export interface AccountBookmarkCount {
  searchText?: string;
}
