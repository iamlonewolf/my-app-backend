export interface AccountCategory {
  searchText?: string;
  page?: number;
  pageSize?: number;
}
