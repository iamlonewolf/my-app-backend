import { Category } from "./Category";
import { Account } from "./Account";
export interface Bookmark {
  _id: number;
  title?: string;
  url?: string;
  account?: Account;
  category?: Category;
  description?: string;
  createdAt?: string;
  updatedAt?: string;
}
