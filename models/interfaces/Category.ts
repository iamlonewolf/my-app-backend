import { Account } from "./Account";

export interface Category {
  _id?: number;
  name?: string;
  description?: string;
  account?: Account;
  dateCreated?: string;
  dateUpdated?: string;
}
